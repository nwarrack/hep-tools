# Your grid certificate has run out...

...you need to renew it....


## How to update your grid certificate *(Presuming you already have one)*
1) Go here to [ca.cern.ch/ca](https://ca.cern.ch/ca)
2) Click the link that says something like, "New Grid User certificate"
3) Choose yourself a certificate ("import") password (this will protect your certificate, you will need it to generate your `usercert.pem` and `userkey.pem` files which you'll need to be able to access the grid).
4) When prompted, download your new certificate (which will be called something like `myCertificate.p12`)
5) Move your certificate to the place from which you intend to acces the grid.
6) On the command line, enter the following (you will be prompted to enter the import password you chose above).
```bash
openssl pkcs12 -in mycert.p12 -clcerts -nokeys -out usercert.pem
```
7) then do the following and, after entering your password again, you will propted to create a new ("PEM") password which will be the one you get asked for when you try to use the grid in the future.
```bash
openssl pkcs12 -in mycert.p12 -nocerts -out userkey.pem
```
8) modify the permissions of your new files, like this:
```bash
chmod 400 userkey.pem
chmod 444 usercert.pem
```
9) Move the `userkey.pem` and `usercert.pem` files to your directory `.globus/` (make it if it doesn't already exist or overwrite any old stuff).

10) You should be good to go - check everything works by doing the following:
```bash
setupATLAS
lsetup rucio
rucio whoami
```
You may have to type something along the lines of
```bash
voms-proxy-init -voms atlas
```
before you are asked for your (new "PEM"!) password.
